package com.masterpik.jumpsrun.items;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class LobbyItems {

    public static ItemStack lobbyItemHub;

    public static void LobbyItemsInit() {
        lobbyItemHub = new ItemStack(Material.IRON_DOOR, 1);
        ItemMeta lobbyItemHubMeta = lobbyItemHub.getItemMeta();
        lobbyItemHubMeta.setDisplayName("§6§l►§r§6§lHUB§r§6§l◄");
        lobbyItemHub.setItemMeta(lobbyItemHubMeta);
    }

}
