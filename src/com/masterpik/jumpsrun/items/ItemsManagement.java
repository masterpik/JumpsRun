package com.masterpik.jumpsrun.items;

import com.masterpik.api.util.UtilPlayer;
import com.masterpik.jumpsrun.Main;
import com.masterpik.jumpsrun.bungee.BungeeWork;
import com.masterpik.jumpsrun.party.JumpsPlayer;
import com.masterpik.jumpsrun.party.PlayerManagement;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;

public class ItemsManagement {

    public static void ItemsInit() {
        JumpsItems.JumpsItemsInit();
        LobbyItems.LobbyItemsInit();
    }

    public static void InventoryInteract(Player player, ItemStack item, Inventory clickInventory) {

        if (!player.getGameMode().equals(GameMode.SPECTATOR)) {

            JumpsPlayer Jplayer = PlayerManagement.playerToJumpsPlayer(player);

            if (Jplayer != null) {

                ArrayList<Player> players = new ArrayList<>();

                int bucle = 0;

                while (bucle < Jplayer.getJumps().getPlayers().size()) {

                    players.add(((JumpsPlayer) Jplayer.getJumps().getPlayers().values().toArray()[bucle]).getPlayer());

                    bucle++;
                }

                if (item.equals(JumpsItems.hidePlayer)) {
                    UtilPlayer.hidePlayer(player, players);

                    player.getInventory().clear(4);

                    BukkitTask time = Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.getInventory().setItem(4, JumpsItems.showPlayer);
                        }
                    }, 20);

                } else if (item.equals(JumpsItems.showPlayer)) {
                    UtilPlayer.showPlayer(player, players);
                    player.getInventory().clear(4);

                    BukkitTask time = Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            player.getInventory().setItem(4, JumpsItems.hidePlayer);
                        }
                    }, 20);
                } else if (item.equals(LobbyItems.lobbyItemHub)) {
                    BungeeWork.sendPlayer(player, "hub");
                }
            }
        }

    }


}
