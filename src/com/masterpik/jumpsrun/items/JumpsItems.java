package com.masterpik.jumpsrun.items;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class JumpsItems {

    public static ItemStack showPlayer;
    public static ItemStack hidePlayer;

    public static void JumpsItemsInit() {
        showPlayer = new ItemStack(Material.STAINED_GLASS_PANE,1,(short)0,(byte)5);

        ItemMeta showPlayerMeta = showPlayer.getItemMeta();
        showPlayerMeta.setDisplayName("§a§lMontrer Joueurs");
        showPlayer.setItemMeta(showPlayerMeta);


        hidePlayer = new ItemStack(Material.STAINED_GLASS_PANE,1,(short)0,(byte)14);

        ItemMeta hidePlayerMeta = hidePlayer.getItemMeta();
        hidePlayerMeta.setDisplayName("§c§lCacher Joueurs");
        hidePlayer.setItemMeta(hidePlayerMeta);

        //14 = rouge
        //5 = vert
    }



}

