package com.masterpik.jumpsrun.bungee;


import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import com.masterpik.api.util.UtilArrayList;
import com.masterpik.api.util.UtilString;
import com.masterpik.jumpsrun.Main;
import com.masterpik.jumpsrun.genbojects.Level;
import com.masterpik.jumpsrun.genbojects.LevelManagement;
import com.masterpik.jumpsrun.party.Jumps;
import com.masterpik.jumpsrun.party.JumpsManagement;
import com.masterpik.jumpsrun.party.JumpsPlayer;
import com.masterpik.jumpsrun.party.PlayerManagement;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.io.IOException;
import java.util.ArrayList;

public class BungeeWork implements PluginMessageListener {

    Plugin plugin;

    public BungeeWork(Plugin instance) {
        plugin = instance;
    }

    public static void sendPlayer(Player player, String serverName) {

        final BungeeUtils bungee = new BungeeUtils(Main.plugin, "gamesCo");
        try {
            bungee.sendRequest(BungeeUtils.MessageType.CONNECT, serverName, null, null, "gamesCo", player);
        } catch (IOException e) {
            e.printStackTrace();
        }



    }

    @Override
    public void onPluginMessageReceived(String channel, Player playerUsed, byte[] message) {
        Bukkit.getLogger().info("MDR TA RE9U UN PTN DE MESSAGE");

        if (channel.equalsIgnoreCase("BungeeCord")) {
            ByteArrayDataInput in = ByteStreams.newDataInput(message);
            String subchannel = in.readUTF();

            if (subchannel.equals("gamesCo")) {

                String getMessage = in.readUTF();
                Bukkit.getLogger().info("Message de la gamesCo : " + getMessage);

                ArrayList<String> data = UtilString.CryptedStringToArrayList(getMessage);

                if (data.get(0).equals("jumpsrun")) {

                    final Player[] player = {Bukkit.getPlayer(data.get(1))};

                    ArrayList<Integer> waiters = new ArrayList<Integer>();

                    waiters.add(Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {

                        @Override
                        public void run() {

                            player[0] = Bukkit.getPlayer(data.get(1));

                            if (player[0] != null && player[0].isOnline()) {

                                if (!PlayerManagement.isRegister(player[0])) {

                                    Level level = LevelManagement.levels.get(""+Integer.parseInt(data.get(2))+"/"+Integer.parseInt(data.get(3)));

                                    int nb = JumpsManagement.getNext(level);

                                    Jumps jump;

                                    if (!JumpsManagement.isPartyExist(Integer.toString(nb))) {
                                        jump = JumpsManagement.createJump(Integer.toString(nb), level);
                                        JumpsManagement.generateJump(jump);
                                    } else {
                                        jump = JumpsManagement.jumps.get(Integer.toString(nb));
                                    }


                                    JumpsPlayer Jplayer = new JumpsPlayer(player[0], jump);
                                    PlayerManagement.players.put(player[0], Jplayer);

                                    jump.getPlayers().put(player[0], Jplayer);

                                    Bukkit.getLogger().info("Joueur  enregistré");
                                    Bukkit.getScheduler().cancelTask(waiters.get(0));
                                } else {
                                    Bukkit.getLogger().info("Joueur DEJA enregistrée :p");
                                    Bukkit.getScheduler().cancelTask(waiters.get(0));
                                }

                            } else {
                                Bukkit.getLogger().info("Joueur pas encore arrivé");
                            }


                        }

                    }, 0, 1));

                }

            }
        }

    }

    public static void sendToGameMessage(Player player, ArrayList<String> arguments, String subChannel) {


        String argumentsString = UtilArrayList.ArrayListToCryptedString(arguments);

        try {
            Main.bungee.sendRequest(BungeeUtils.MessageType.FORWARD, arguments.get(0), null, argumentsString, subChannel, Bukkit.getServer());
            Bukkit.getLogger().info("MESSAGE ENVOY2");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static void sendToGameMessage(Player player, ArrayList<String> arguments) {


        sendToGameMessage(player, arguments, "gamesCo");

    }

}
