package com.masterpik.jumpsrun.genbojects;

import com.masterpik.connect.enums.StatsPiks;

import java.util.ArrayList;

public class Level {

    int type;
    int difficulty;

    int heightLvl0Prct;
    int heightLvl1Prct;

    int diff1Prct;
    int diff2Prct;
    int diff3Prct;

    int minLgt;
    int maxLgt;

    ArrayList<MaterialData> materials;

    StatsPiks typ;
    StatsPiks lvl;

    public Level(int type, int difficulty, int heightLvl0Prct, int heightLvl1Prct, int diff1Prct, int diff2Prct, int diff3Prct, int minLgt, int maxLgt, ArrayList<MaterialData> materials) {
        this.type = type;
        this.difficulty = difficulty;
        this.heightLvl0Prct = heightLvl0Prct;
        this.heightLvl1Prct = heightLvl1Prct;
        this.diff1Prct = diff1Prct;
        this.diff2Prct = diff2Prct;
        this.diff3Prct = diff3Prct;
        this.minLgt = minLgt;
        this.maxLgt = maxLgt;
        this.materials = materials;

        switch (type) {

            case 1:
                this.typ = StatsPiks.JUMPSRUN_GLASS;
                break;
            case 2:
                this.typ = StatsPiks.JUMPSRUN_ICE;
                break;
            case 3:
                this.typ = StatsPiks.JUMPSRUN_FLOWER;
                break;
            case 4:
                this.typ = StatsPiks.JUMPSRUN_STICK;
            default:
                break;
        }

        switch (difficulty) {
            case 1:
                this.lvl = StatsPiks.JUMPSRUN_LVL_1;
                break;
            case 2:
                this.lvl = StatsPiks.JUMPSRUN_LVL_2;
                break;
            case 3:
                this.lvl = StatsPiks.JUMPSRUN_LVL_3;
                break;
            default:
                break;
        }

    }

    public int getType() {
        return type;
    }
    public void setType(int type) {
        this.type = type;
    }

    public int getDifficulty() {
        return difficulty;
    }
    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public int getHeightLvl0Prct() {
        return heightLvl0Prct;
    }
    public void setHeightLvl0Prct(int heightLvl0Prct) {
        this.heightLvl0Prct = heightLvl0Prct;
    }

    public int getHeightLvl1Prct() {
        return heightLvl1Prct;
    }
    public void setHeightLvl1Prct(int heightLvl1Prct) {
        this.heightLvl1Prct = heightLvl1Prct;
    }

    public int getDiff1Prct() {
        return diff1Prct;
    }
    public void setDiff1Prct(int diff1Prct) {
        this.diff1Prct = diff1Prct;
    }

    public int getDiff2Prct() {
        return diff2Prct;
    }
    public void setDiff2Prct(int diff2Prct) {
        this.diff2Prct = diff2Prct;
    }

    public int getDiff3Prct() {
        return diff3Prct;
    }
    public void setDiff3Prct(int diff3Prct) {
        this.diff3Prct = diff3Prct;
    }

    public int getMinLgt() {
        return minLgt;
    }
    public void setMinLgt(int minLgt) {
        this.minLgt = minLgt;
    }

    public int getMaxLgt() {
        return maxLgt;
    }
    public void setMaxLgt(int maxLgt) {
        this.maxLgt = maxLgt;
    }

    public ArrayList<MaterialData> getMaterials() {
        return materials;
    }
    public void setMaterials(ArrayList<MaterialData> materials) {
        this.materials = materials;
    }

    public StatsPiks getTyp() {
        return typ;
    }

    public void setTyp(StatsPiks typ) {
        this.typ = typ;
    }

    public StatsPiks getLvl() {
        return lvl;
    }

    public void setLvl(StatsPiks lvl) {
        this.lvl = lvl;
    }
}
