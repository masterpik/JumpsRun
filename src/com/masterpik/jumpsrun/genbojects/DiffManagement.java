package com.masterpik.jumpsrun.genbojects;

import java.util.ArrayList;
import java.util.HashMap;

public class DiffManagement {

    public static HashMap<Integer, Diff> diffs;

    public static void DiffInit() {

        diffs = new HashMap<>();

        diffs.put(1, new Diff());

        diffs.get(1).getBlocks().put(0, new ArrayList<>());
        diffs.get(1).getBlocks().get(0).add(new JumpBlock(-2, 1));
        diffs.get(1).getBlocks().get(0).add(new JumpBlock(-2, 0));
        diffs.get(1).getBlocks().get(0).add(new JumpBlock(-2, -1));
        diffs.get(1).getBlocks().get(0).add(new JumpBlock(-3, 1));
        diffs.get(1).getBlocks().get(0).add(new JumpBlock(-3, 0));
        diffs.get(1).getBlocks().get(0).add(new JumpBlock(-3, -1));

        diffs.get(1).getBlocks().put(1, new ArrayList<>());
        diffs.get(1).getBlocks().get(1).add(new JumpBlock(-2, 1));
        diffs.get(1).getBlocks().get(1).add(new JumpBlock(-2, 0));
        diffs.get(1).getBlocks().get(1).add(new JumpBlock(-2, -1));



        diffs.put(2, new Diff());

        diffs.get(2).getBlocks().put(0, new ArrayList<>());
        diffs.get(2).getBlocks().get(0).add(new JumpBlock(-3, 2));
        diffs.get(2).getBlocks().get(0).add(new JumpBlock(-3, -2));
        diffs.get(2).getBlocks().get(0).add(new JumpBlock(-4, 1));
        diffs.get(2).getBlocks().get(0).add(new JumpBlock(-4, 0));
        diffs.get(2).getBlocks().get(0).add(new JumpBlock(-4, -1));

        diffs.get(2).getBlocks().put(1, new ArrayList<>());
        diffs.get(2).getBlocks().get(1).add(new JumpBlock(-3, 2));
        diffs.get(2).getBlocks().get(1).add(new JumpBlock(-3, 1));
        diffs.get(2).getBlocks().get(1).add(new JumpBlock(-3, 0));
        diffs.get(2).getBlocks().get(1).add(new JumpBlock(-3, -1));
        diffs.get(2).getBlocks().get(1).add(new JumpBlock(-3, -2));



        diffs.put(3, new Diff());

        diffs.get(3).getBlocks().put(0, new ArrayList<>());
        diffs.get(3).getBlocks().get(0).add(new JumpBlock(-4, 3));
        diffs.get(3).getBlocks().get(0).add(new JumpBlock(-4, 2));
        diffs.get(3).getBlocks().get(0).add(new JumpBlock(-4, -3));
        diffs.get(3).getBlocks().get(0).add(new JumpBlock(-4, -2));
        diffs.get(3).getBlocks().get(0).add(new JumpBlock(-5, 2));
        diffs.get(3).getBlocks().get(0).add(new JumpBlock(-5, 1));
        diffs.get(3).getBlocks().get(0).add(new JumpBlock(-5, 0));
        diffs.get(3).getBlocks().get(0).add(new JumpBlock(-5, -1));
        diffs.get(3).getBlocks().get(0).add(new JumpBlock(-5, -2));

        diffs.get(3).getBlocks().put(1, new ArrayList<>());
        diffs.get(3).getBlocks().get(1).add(new JumpBlock(-4, 2));
        diffs.get(3).getBlocks().get(1).add(new JumpBlock(-4, 1));
        diffs.get(3).getBlocks().get(1).add(new JumpBlock(-4, 0));
        diffs.get(3).getBlocks().get(1).add(new JumpBlock(-4, -1));
        diffs.get(3).getBlocks().get(1).add(new JumpBlock(-4, -2));

    }

}
