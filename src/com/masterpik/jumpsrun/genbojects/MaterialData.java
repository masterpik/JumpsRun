package com.masterpik.jumpsrun.genbojects;

import org.bukkit.Material;

public class MaterialData {

    Material material;
    Byte data;


    public MaterialData(Material Pmaterial, Byte Pdata) {
        this.material = Pmaterial;
        this.data = Pdata;
    }

    public MaterialData(Material Pmaterial, int Pdata) {
        this.material = Pmaterial;
        this.data = (byte) Pdata;
    }

    public Material getMaterial() {
        return material;
    }
    public void setMaterial(Material Pmaterial) {
        this.material = Pmaterial;
    }

    public Byte getData() {
        return data;
    }
    public void setData(Byte Pdata) {
        this.data = Pdata;
    }
}
