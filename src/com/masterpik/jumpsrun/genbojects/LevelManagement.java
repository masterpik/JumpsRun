package com.masterpik.jumpsrun.genbojects;

import java.util.HashMap;

public class LevelManagement {

    public static HashMap<String, Level> levels;

    public static void LevelsInit() {
        levels = new HashMap<>();
        initLevel1();
        initLevel2();
        initLevel3();
        initLevel4();
    }

    public static void initLevel1() {
        initLevel11();
        initLevel12();
        initLevel13();
    }

    public static void initLevel2() {
        initLevel21();
        initLevel22();
        initLevel23();
    }

    public static void initLevel3() {
        initLevel31();
        initLevel32();
        initLevel33();
    }

    public static void initLevel4() {
        initLevel41();
        initLevel42();
        initLevel43();
    }


    public static void initLevel11() {
        levels.put("1/1", new Level(1, 1, 20, 80, 40, 60, 0, 10, 20, MaterialDataManagement.getMaterials(1)));
    }

    public static void initLevel12() {
        levels.put("1/2", new Level(1, 2, 10, 90, 25, 70, 5, 15, 25, MaterialDataManagement.getMaterials(1)));
    }

    public static void initLevel13() {
        levels.put("1/3", new Level(1, 3, 5, 95, 5, 70, 25, 20, 30, MaterialDataManagement.getMaterials(1)));
    }


    public static void initLevel21() {
        levels.put("2/1", new Level(2, 1, 20, 80, 40, 60, 0, 10, 20, MaterialDataManagement.getMaterials(2)));
    }

    public static void initLevel22() {
        levels.put("2/2", new Level(2, 2, 10, 90, 25, 70, 5, 15, 25, MaterialDataManagement.getMaterials(2)));
    }

    public static void initLevel23() {
        levels.put("2/3", new Level(2, 3, 5, 95, 5, 70, 25, 20, 30, MaterialDataManagement.getMaterials(2)));
    }


    public static void initLevel31() {
        levels.put("3/1", new Level(3, 1, 20, 80, 90, 10, 0, 5, 15, MaterialDataManagement.getMaterials(3)));
    }

    public static void initLevel32() {
        levels.put("3/2", new Level(3, 2, 10, 90, 70, 30, 0, 10, 20, MaterialDataManagement.getMaterials(3)));
    }

    public static void initLevel33() {
        levels.put("3/3", new Level(3, 3, 5, 95, 50, 50, 0, 15, 25, MaterialDataManagement.getMaterials(3)));
    }


    public static void initLevel41() {
        levels.put("4/1", new Level(4, 1, 20, 80, 90, 10, 0, 5, 15, MaterialDataManagement.getMaterials(4)));
    }

    public static void initLevel42() {
        levels.put("4/2", new Level(4, 2, 10, 90, 70, 30, 0, 10, 20, MaterialDataManagement.getMaterials(4)));
    }

    public static void initLevel43() {
        levels.put("4/3", new Level(4, 3, 5, 95, 50, 50, 0, 15, 25, MaterialDataManagement.getMaterials(4)));
    }


}