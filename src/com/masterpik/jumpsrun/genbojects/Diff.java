package com.masterpik.jumpsrun.genbojects;

import java.util.ArrayList;
import java.util.HashMap;

public class Diff {


    HashMap<Integer,ArrayList<JumpBlock>> blocks;

    public Diff() {
        blocks = new HashMap<>();
    }

    public HashMap<Integer, ArrayList<JumpBlock>> getBlocks() {
        return blocks;
    }
    public void setBlocks(HashMap<Integer, ArrayList<JumpBlock>> Pblocks) {
        this.blocks = Pblocks;
    }
}
