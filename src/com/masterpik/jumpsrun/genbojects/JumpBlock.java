package com.masterpik.jumpsrun.genbojects;

public class JumpBlock {

    int pX;
    int pZ;

    public JumpBlock(int PpX, int PpZ) {
        this.pX = PpX;
        this.pZ = PpZ;
    }

    public int getpX() {
        return pX;
    }
    public void setpX(int PpX) {
        this.pX = PpX;
    }

    public int getpZ() {
        return pZ;
    }
    public void setpZ(int PpZ) {
        this.pZ = PpZ;
    }
}

