package com.masterpik.jumpsrun.genbojects;

import com.masterpik.jumpsrun.*;
import com.masterpik.jumpsrun.party.Jumps;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.material.FlowerPot;

import java.util.*;

public class Generator {

    public static void generateAJumps(Jumps jump) {

        jump.setLenght(generateLenght(jump));

        generateBlocks(jump);


    }

    public static void generateBlocks(Jumps jump) {

        jump.getBlocks().clear();

        int bucle = 0;
        int lenght = jump.getLenght();

        while (bucle < lenght) {

            if (bucle != (lenght - 1)) {
                generateABlock(jump, bucle, null);
            } else {
                generateABlock(jump, bucle, Settings.endBlock);
            }

            bucle++;
        }

    }

    public static void generateABlock(Jumps jump, int bclPos, Material specialType) {

        Location lastBlock = Settings.firstLocation.clone();
        lastBlock.setWorld(jump.getWorld());

        if (bclPos > 0) {
            lastBlock = jump.getBlocks().get(bclPos - 1).getLocation();
        }

        Location newBlock = lastBlock.clone();

        int height;

        if ((jump.getLevel().getType() == 3 || jump.getLevel().getType() == 4) && specialType != null) {
            height = 0;
        } else {
            HashMap<Integer, Integer> heights = new HashMap<>();
            heights.put(0, jump.getLevel().getHeightLvl0Prct());
            heights.put(1, jump.getLevel().getHeightLvl1Prct());

            height = generateRandom(heights);
        }

        newBlock.add(0, height, 0);

        int diff;

        if ((jump.getLevel().getType() == 3 || jump.getLevel().getType() == 4) && specialType != null) {
            diff = 1;
        } else {
            HashMap<Integer, Integer> diffs = new HashMap<>();
            diffs.put(1, jump.getLevel().getDiff1Prct());
            diffs.put(2, jump.getLevel().getDiff2Prct());
            diffs.put(3, jump.getLevel().getDiff3Prct());

            diff = generateRandom(diffs);
        }

        ArrayList<JumpBlock> blockHD = (ArrayList<JumpBlock>) DiffManagement.diffs.get(diff).getBlocks().get(height).clone();

        Collections.shuffle(blockHD);

        newBlock.add(blockHD.get(0).getpX(), 0, blockHD.get(0).getpZ());


        Block newRBlock = newBlock.getBlock();

        if (specialType == null) {

            //ArrayList<MaterialData> materials = (ArrayList<MaterialData>) MaterialDataManagement.materials.clone();
            ArrayList<MaterialData> materials = (ArrayList<MaterialData>) jump.getLevel().getMaterials();

            Collections.shuffle(materials);

            newRBlock.setTypeIdAndData(materials.get(0).getMaterial().getId(), materials.get(0).getData(), false);


        } else {
            newRBlock.setType(specialType);
        }

        jump.getBlocks().add(newRBlock);


    }

    public static Integer generateLenght(Jumps jump) {
        ArrayList<Integer> lenghts = new ArrayList<>();

        int lenghtBcl = jump.getLevel().getMinLgt();

        while (lenghtBcl <= jump.getLevel().getMaxLgt()) {

            lenghts.add(lenghtBcl);

            lenghtBcl ++;
        }

        Collections.shuffle(lenghts);

        return lenghts.get(0);

    }

    public static Integer generateRandom(HashMap<Integer, Integer> pos) {

        List<Integer> loto = new ArrayList<>();

        int bucle1 = 0;
        int bucle2 = 0;

        Iterator<Integer> it =  pos.keySet().iterator();

        //Bukkit.getLogger().info("pos.size : "+Integer.toString(pos.size()));

        while (bucle1 < pos.size()) {


            int thisInt = it.next();

            //Bukkit.getLogger().info("thisInt : "+thisInt);

            bucle2 = 0;

            while (bucle2 < pos.get(thisInt)) {

                loto.add(thisInt);

                bucle2++;
            }


            bucle1++;
        }

        /*boolean ok = false;

        int pass = 0;

        int bucle1 = 1;

        while (!ok) {

            if (bucle1 == pos.get(pos.keySet().toArray()[pass])) {
                bucle1 = 0;
                pass++;
            }

            if (pass < pos.keySet().toArray().length) {
                loto.add((Integer) pos.keySet().toArray()[pass]);
            }
            else {
                ok = true;
            }

            bucle1++;
        }*/

        Collections.shuffle(loto);

        //Bukkit.getLogger().info("pos.size : "+pos.size()+" || loto.size : "+loto.size());

        return loto.get(0);
    }

}
