package com.masterpik.jumpsrun.genbojects;

import org.bukkit.Material;

import java.util.ArrayList;

public class MaterialDataManagement {

    //public static ArrayList<MaterialData> materials;

    /*public static void MaterialDataInit() {
        materials = new ArrayList<>();

        //materials.add(new MaterialData(Material.GLASS, 0));

        int bucle1 = 0;

        materials.add(new MaterialData(Material.STAINED_GLASS, 11));
        materials.add(new MaterialData(Material.STAINED_GLASS, 6));
        materials.add(new MaterialData(Material.STAINED_GLASS, 5));
        materials.add(new MaterialData(Material.STAINED_GLASS, 4));
        materials.add(new MaterialData(Material.STAINED_GLASS, 2));
        materials.add(new MaterialData(Material.STAINED_GLASS, 0));
        materials.add(new MaterialData(Material.STAINED_GLASS, 1));
        materials.add(new MaterialData(Material.STAINED_GLASS, 15));
        materials.add(new MaterialData(Material.STAINED_GLASS, 14));

        /*while (bucle1 <= 15) {

            materials.add(new MaterialData(Material.STAINED_GLASS, bucle1));
            materials.add(new MaterialData(Material.STAINED_CLAY, bucle1));
            materials.add(new MaterialData(Material.WOOL, bucle1));

            bucle1++;
        }*/

        /*materials.add(new MaterialData(Material.QUARTZ_BLOCK, 0));
        materials.add(new MaterialData(Material.SEA_LANTERN, 0));*/



    //}

    public static ArrayList<MaterialData> getMaterials(int type) {
        ArrayList<MaterialData> materials = new ArrayList<>();


        if(type == 1) {
            materials.add(new MaterialData(Material.STAINED_GLASS, 11));
            materials.add(new MaterialData(Material.STAINED_GLASS, 6));
            materials.add(new MaterialData(Material.STAINED_GLASS, 5));
            materials.add(new MaterialData(Material.STAINED_GLASS, 4));
            materials.add(new MaterialData(Material.STAINED_GLASS, 2));
            materials.add(new MaterialData(Material.STAINED_GLASS, 0));
            materials.add(new MaterialData(Material.STAINED_GLASS, 1));
            materials.add(new MaterialData(Material.STAINED_GLASS, 15));
            materials.add(new MaterialData(Material.STAINED_GLASS, 14));
        }
        else if (type == 2) {
            materials.add(new MaterialData(Material.PACKED_ICE, 0));
            materials.add(new MaterialData(Material.ICE, 0));
        }
        else if (type == 3) {
            materials.add(new MaterialData(Material.FLOWER_POT, 0));
        } else if (type == 4) {
            materials.add(new MaterialData(Material.STAINED_GLASS_PANE, 11));
            materials.add(new MaterialData(Material.STAINED_GLASS_PANE, 6));
            materials.add(new MaterialData(Material.STAINED_GLASS_PANE, 5));
            materials.add(new MaterialData(Material.STAINED_GLASS_PANE, 4));
            materials.add(new MaterialData(Material.STAINED_GLASS_PANE, 2));
            materials.add(new MaterialData(Material.STAINED_GLASS_PANE, 0));
            materials.add(new MaterialData(Material.STAINED_GLASS_PANE, 1));
            materials.add(new MaterialData(Material.STAINED_GLASS_PANE, 15));
            materials.add(new MaterialData(Material.STAINED_GLASS_PANE, 14));
            materials.add(new MaterialData(Material.THIN_GLASS, 0));
        }


        return materials;
    }

}
