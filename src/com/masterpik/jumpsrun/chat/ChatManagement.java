package com.masterpik.jumpsrun.chat;

import com.masterpik.jumpsrun.party.JumpsPlayer;
import com.masterpik.jumpsrun.party.PlayerManagement;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class ChatManagement {

    public static String transformGlobal(String message, Player player) {
        String nuew;

        nuew = ""+Messages.main+""+Messages.global+"§6§l"+player.getName()+"§l ➜§r "+message+"";

        return nuew;
    }

    public static void send(String message, Player player) {
        player.sendMessage(message);
    }

    public static void sendToGlobal(String message, Player player) {

        JumpsPlayer Jplayer = PlayerManagement.playerToJumpsPlayer(player);

        if (Jplayer != null) {

            ArrayList<JumpsPlayer> players = new ArrayList<>();
            players.addAll(Jplayer.getJumps().getPlayers().values());

            int bucle = 0;

            while (bucle < players.size()) {
                players.get(bucle).getPlayer().sendMessage(message);
                bucle++;
            }
        }

    }


    public static void sendMessage(String message, Player player) {
        if (message.charAt(0) == '@'
                || message.charAt(0) == '!') {
            message = message.substring(1);
        }
        message = transformGlobal(message, player);
        sendToGlobal(message, player);
    }

    public static void brodcasteMessage(String message, Player player) {
        message = ""+Messages.main+""+Messages.global+""+message;
        sendToGlobal(message, player);
    }

}
