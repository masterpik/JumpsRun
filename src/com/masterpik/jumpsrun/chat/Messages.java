package com.masterpik.jumpsrun.chat;

import com.masterpik.messages.spigot.Api;

public class Messages {

    public static String masterpik = Api.getString("general.masterpik.scoreboard.header");

    public static String main = Api.getString("jumpsrun.chat.mainPrefix");
    public static String global = Api.getString("general.all.miniGames.chat.allPrefix");

    public static String Join = Api.getString("general.all.miniGames.chat.playerJoin");
    public static String Quit = Api.getString("general.all.miniGames.chat.playerQuit");

    public static String win1 = Api.getString("general.all.miniGames.detection.playerWin.1");
    public static String win2 = Api.getString("general.all.miniGames.detection.playerWin.2");

    public static String countRebour = Api.getString("general.all.miniGames.coolDown.partyStarting");
    public static String partyStart = Api.getString("general.all.miniGames.coolDown.partyStart");
    public static String partyStartTitle = Api.getString("general.all.miniGames.title.partyStart");

    public static String endParty = Messages.main+Api.getString("general.all.miniGames.detection.end");

    public static String bloc1 = "▇";

    public static String notEnoughtPlayers = Api.getString("general.all.miniGames.coolDown.notEnoughtPlayers");

    public static String startingMessageDescription = Api.getString("jumpsrun.tutorial.startGame");

    public static String statsRegister = Api.getString("stats.register");

}
