package com.masterpik.jumpsrun;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;

public class Settings {

    public static Location firstLocation;
    public static Location lobbyLocation;

    public static int maxPlayers;

    public static Material endBlock;

    public static int startCountDown;
    public static int forceStartCountDown;


    public static float volumeSound = (float) 1.0;
    public static float pitchSound = (float) 1.0;


    public static void SettingsInit() {

        firstLocation = new Location(Bukkit.getWorld("world"), 0.5, 100, 0.5, 90, 0);

        lobbyLocation = new Location(Bukkit.getWorld("lobby"), 0.5, 101, 0.5, 0, -90);

        maxPlayers = 10;

        endBlock = Material.BEACON;

        startCountDown = 30;
        forceStartCountDown = 10;
    }



}
