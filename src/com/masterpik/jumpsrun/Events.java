package com.masterpik.jumpsrun;

import com.masterpik.api.util.UtilPlayer;
import com.masterpik.jumpsrun.chat.ChatManagement;
import com.masterpik.jumpsrun.items.ItemsManagement;
import com.masterpik.jumpsrun.party.Jumps;
import com.masterpik.jumpsrun.party.JumpsManagement;
import com.masterpik.jumpsrun.genbojects.LevelManagement;
import com.masterpik.jumpsrun.party.JumpsPlayer;
import com.masterpik.jumpsrun.party.PlayerManagement;
import com.masterpik.jumpsrun.scoreboard.ScoreboardManagement;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.event.player.*;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;

public class Events implements Listener {

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void PlayerMoveEvent(PlayerMoveEvent event) {
        Player player = event.getPlayer();

        if (!player.getGameMode().equals(GameMode.SPECTATOR)) {

            if (PlayerManagement.isRegister(player)) {
                JumpsPlayer Jplayer = PlayerManagement.playerToJumpsPlayer(player);
                Jumps jump = Jplayer.getJumps();

                int minY = Settings.firstLocation.getBlockY() - 1;

                if (Jplayer.getLastBlock() != null) {
                    minY = Jplayer.getLastBlock().getY() - 1;
                }


                if (!player.getGameMode().equals(GameMode.SPECTATOR)) {

                    if (player.getLocation().getBlockY() <= minY
                            && !player.isOnGround()) {


                        if (!player.getWorld().equals(Bukkit.getWorld("lobby"))) {

                            Jplayer.setLastBlock(null);
                            ScoreboardManagement.setAPlayerScoreToParty(jump, Jplayer, 0);
                            UtilPlayer.setXpBarCount(player, 0, 0.0F);

                            Location tpLoc = Settings.firstLocation.clone();
                            tpLoc.setWorld(Jplayer.getJumps().getWorld());
                            tpLoc.add(0, 1, 0);
                            Jplayer.getPlayer().teleport(tpLoc);
                            Jplayer.getPlayer().playSound(Jplayer.getPlayer().getLocation(), Sound.ENTITY_WITHER_HURT, Settings.volumeSound, Settings.pitchSound);
                        } else {
                            Location tp = Settings.lobbyLocation.clone();
                            tp.setYaw(player.getLocation().getYaw());
                            tp.setPitch(player.getLocation().getPitch());
                            Jplayer.getPlayer().teleport(tp);
                        }

                    }


                    Block block = player.getLocation().getBlock().getRelative(BlockFace.DOWN);
                    Block blockin = player.getLocation().getBlock();

                    if (block.getType().equals(Material.BEACON)
                            && !Jplayer.isWin()
                            && Jplayer.getJumps().getWinner() == null) {
                        PlayerManagement.playerWin(Jplayer);
                        Jplayer.getPlayer().playSound(Jplayer.getPlayer().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, Settings.volumeSound, Settings.pitchSound);
                    }

                    if (player.getWorld().equals(jump.getWorld())) {
                        if (Jplayer.getLastBlock() != block && jump.getBlocks().contains(block)/* && block.getType().isSolid()*/) {
                            if (Jplayer.getLastBlock() == null
                                    || !Jplayer.getLastBlock().equals(block)) {
                                Jplayer.getPlayer().playSound(Jplayer.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, Settings.volumeSound, Settings.pitchSound);
                            }
                            Jplayer.setLastBlock(block);
                            int pos = jump.getBlocks().indexOf(block) + 1;
                            float prct = (pos * 100.0F) / jump.getBlocks().size();
                            ScoreboardManagement.setAPlayerScoreToParty(jump, Jplayer, pos);
                            UtilPlayer.setXpBarCount(player, pos, prct);

                        } else if (Jplayer.getLastBlock() != blockin && jump.getBlocks().contains(blockin)) {
                            if (Jplayer.getLastBlock() == null
                                    || !Jplayer.getLastBlock().equals(blockin)) {
                                Jplayer.getPlayer().playSound(Jplayer.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, Settings.volumeSound, Settings.pitchSound);
                            }
                            Jplayer.setLastBlock(blockin);
                            int pos = jump.getBlocks().indexOf(blockin) + 1;
                            float prct = (pos * 100.0F) / jump.getBlocks().size();
                            ScoreboardManagement.setAPlayerScoreToParty(jump, Jplayer, pos);
                            UtilPlayer.setXpBarCount(player, pos, prct);

                        }
                    }
                }


            }
        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void PlayerJoinEvent(PlayerJoinEvent event) {

        Player player = event.getPlayer();

        event.setJoinMessage(null);

        if (PlayerManagement.isRegister(player)) {
            PlayerManagement.playerJoin(player);
        }
        else {
            player.teleport(Settings.lobbyLocation);

            ArrayList<Integer> waiters = new ArrayList<Integer>();
            waiters.add(Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                @Override
                public void run() {
                    if (PlayerManagement.isRegister(player)) {

                        PlayerManagement.playerJoin(player);

                        Bukkit.getScheduler().cancelTask(waiters.get(0));

                    } else {
                        player.teleport(Settings.lobbyLocation);
                    }
                }
            }, 0, 1));
        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void PlayerQuitEvent(PlayerQuitEvent event) {
        Player player = event.getPlayer();

        event.setQuitMessage(null);
        PlayerManagement.playerQuit(player);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void FoodLevelChangeEvent(FoodLevelChangeEvent event) {
        event.setCancelled(true);
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void AsyncPlayerChatEvent(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        String message = event.getMessage();

        if (PlayerManagement.isRegister(player)) {

            ChatManagement.sendMessage(message, player);
        }

        event.setCancelled(true);

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void EntityDamageEvent(EntityDamageEvent event) {
        if (event.getEntity() instanceof Player) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void PlayerInteractEvent(PlayerInteractEvent event) {
        if (event.getClickedBlock() != null
            && event.getClickedBlock().getType().equals(Material.BEACON)) {
            event.setCancelled(true);
        }
        event.setCancelled(true);

        ItemsManagement.InventoryInteract(event.getPlayer(), event.getPlayer().getItemInHand(),event.getPlayer().getInventory());

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void InventoryClickEvent(InventoryClickEvent event) {
        ItemsManagement.InventoryInteract((Player) event.getWhoClicked(), event.getCurrentItem(), event.getClickedInventory());
        event.setCancelled(true);
    }
    @EventHandler(priority = EventPriority.HIGHEST)
    public void PlayerDropItemEvent(PlayerDropItemEvent event) {
        event.setCancelled(true);
    }

    /*@EventHandler(ignoreCancelled = true,priority = EventPriority.HIGHEST)
    public void ChunkLoadEvent(ChunkLoadEvent event) {
        if(event.isNewChunk()) {
            event.getChunk().unload(false, false);
            Bukkit.getLogger().info("I block a new chunk <3");
        }
    }*/

    @EventHandler(priority = EventPriority.HIGHEST)
    public void WeatherChangeEvent(WeatherChangeEvent event) {
        if (event.toWeatherState()) {
            event.setCancelled(true);
        }
    }

}
