package com.masterpik.jumpsrun.scoreboard;

import com.masterpik.jumpsrun.Main;
import com.masterpik.jumpsrun.chat.Messages;
import com.masterpik.jumpsrun.party.Jumps;
import com.masterpik.jumpsrun.party.JumpsPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

import java.util.ArrayList;

public class ScoreboardManagement {

    public static int howMainScore = 99;

    public static Scoreboard getMain(Player player) {
        Scoreboard scoreboard = Main.scoreboard.getNewScoreboard();

        Objective sidebar = scoreboard.registerNewObjective("sidebar", "dummy");

        sidebar.setDisplayName(Messages.masterpik);
        sidebar.setDisplaySlot(DisplaySlot.SIDEBAR);

        Score sidebarScore1 = sidebar.getScore(" ");
        sidebarScore1.setScore(howMainScore);

        Score sidebarScore2 = sidebar.getScore(Messages.main);
        sidebarScore2.setScore(howMainScore - 1);

        Score sidebarScore3 = sidebar.getScore("  ");
        sidebarScore3.setScore(howMainScore - 2);

        scoreboard.registerNewTeam(player.getName());
        scoreboard.getTeam(player.getName()).setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.NEVER);
        scoreboard.getTeam(player.getName()).addEntry(player.getName());



        return scoreboard;
    }

    public static Scoreboard addAPlayer(Scoreboard scoreboard, JumpsPlayer player) {

        Objective objective = scoreboard.getObjective("sidebar");

        Score playerScore = objective.getScore(player.getPlayer().getName());
        playerScore.setScore(0);

        return scoreboard;
    }
    public static void addAPlayerToParty(Jumps jump, JumpsPlayer player) {
        ArrayList<JumpsPlayer> playersParty = new ArrayList<>();
        playersParty.addAll(jump.getPlayers().values());

        int bucle1 = 0;

        while (bucle1 < playersParty.size()) {

            player.getPlayer().setScoreboard(addAPlayer(player.getPlayer().getScoreboard(),playersParty.get(bucle1)));
            playersParty.get(bucle1).getPlayer().setScoreboard(addAPlayer(playersParty.get(bucle1).getPlayer().getScoreboard(), player));

            bucle1 ++;
        }
    }

    public static Scoreboard setScoreToAPlayer(Scoreboard scoreboard, JumpsPlayer player, int score) {

        Objective objective = scoreboard.getObjective("sidebar");

        Score playerScore = objective.getScore(player.getPlayer().getName());
        playerScore.setScore(score);

        return scoreboard;
    }
    public static void setAPlayerScoreToParty(Jumps jump, JumpsPlayer player, int score) {
        ArrayList<JumpsPlayer> playersParty = new ArrayList<>();
        playersParty.addAll(jump.getPlayers().values());

        int bucle1 = 0;

        while (bucle1 < playersParty.size()) {

            playersParty.get(bucle1).getPlayer().setScoreboard(setScoreToAPlayer(playersParty.get(bucle1).getPlayer().getScoreboard(), player, score));

            bucle1 ++;
        }
    }

    public static Scoreboard removeAPlayer(Scoreboard scoreboard, JumpsPlayer player) {

        scoreboard.resetScores(player.getPlayer().getName());

        return scoreboard;
    }
    public static void removeAPlayerToParty(Jumps jump, JumpsPlayer player) {
        ArrayList<JumpsPlayer> playersParty = new ArrayList<>();
        playersParty.addAll(jump.getPlayers().values());

        int bucle1 = 0;

        while (bucle1 < playersParty.size()) {

            playersParty.get(bucle1).getPlayer().setScoreboard(removeAPlayer(playersParty.get(bucle1).getPlayer().getScoreboard(), player));

            bucle1 ++;
        }
    }

}
