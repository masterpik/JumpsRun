package com.masterpik.jumpsrun;

import com.masterpik.api.util.UtilWorld;
import com.masterpik.games.settings.GeneralSettings;
import com.masterpik.games.settings.LobbySettings;
import com.masterpik.jumpsrun.bungee.BungeeUtils;
import com.masterpik.jumpsrun.bungee.BungeeWork;
import com.masterpik.jumpsrun.commands.PlayersCommands;
import com.masterpik.jumpsrun.genbojects.DiffManagement;
import com.masterpik.jumpsrun.items.ItemsManagement;
import com.masterpik.jumpsrun.party.JumpsManagement;
import com.masterpik.jumpsrun.genbojects.LevelManagement;
import com.masterpik.jumpsrun.genbojects.MaterialDataManagement;
import com.masterpik.jumpsrun.party.PlayerManagement;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.ScoreboardManager;

import java.util.ArrayList;

public class Main extends JavaPlugin {

    public static Plugin plugin;
    public static BungeeUtils bungee;
    public static ScoreboardManager scoreboard;

    public static ArrayList<Player> spectators;

    @Override
    public void onEnable() {

        plugin = this;
        scoreboard = Bukkit.getScoreboardManager();

        Bukkit.getPluginManager().registerEvents(new Events(), this);
        getCommand("start").setExecutor(new PlayersCommands());
        getCommand("spectate").setExecutor(new PlayersCommands());

        Main.bungee = new BungeeUtils(this, "gamesCo");
        Bukkit.getMessenger().registerIncomingPluginChannel(this, "BungeeCord", new BungeeWork(this));

        UtilWorld.loadWorld("lobby");
        JumpsManagement.initGamerule(Bukkit.getWorld("lobby"));

        Settings.SettingsInit();

        LevelManagement.LevelsInit();
        DiffManagement.DiffInit();
        //MaterialDataManagement.MaterialDataInit();
        JumpsManagement.JumpsInit();
        PlayerManagement.PlayersInit();

        ItemsManagement.ItemsInit();

        spectators = new ArrayList<>();

        GeneralSettings.setCanSwapItem(false);
        LobbySettings.setLobbyWorld(Settings.lobbyLocation.getWorld());
        LobbySettings.setCanSwapItem(false);

    }

}
