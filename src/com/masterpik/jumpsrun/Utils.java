package com.masterpik.jumpsrun;

import com.comphenix.protocol.wrappers.EnumWrappers;
import com.masterpik.api.json.ChatText;
import com.masterpik.api.nms.player.Title;
import com.masterpik.api.util.UtilChat;
import com.masterpik.api.util.UtilPlayer;
import com.masterpik.api.util.UtilTitle;
import com.masterpik.jumpsrun.party.Jumps;
import com.masterpik.jumpsrun.party.JumpsPlayer;
import com.masterpik.jumpsrun.party.PlayerManagement;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class Utils {

    public static void showPlayersPlayer(Player player) {

        ArrayList<Player> allPlayers = new ArrayList<Player>();
        allPlayers.addAll(Bukkit.getOnlinePlayers());

        int bucle1 = 0;

        while (bucle1 < allPlayers.size()) {
            player.hidePlayer(allPlayers.get(bucle1));
            allPlayers.get(bucle1).hidePlayer(player);
            bucle1 ++;
        }

        ArrayList<JumpsPlayer> partyPlayers = new ArrayList<>();
        JumpsPlayer Jplayer = PlayerManagement.playerToJumpsPlayer(player);

        if (Jplayer != null) {

            partyPlayers.addAll(Jplayer.getJumps().getPlayers().values());

            int bucle2 = 0;

            while (bucle2 < partyPlayers.size()) {

                player.showPlayer(partyPlayers.get(bucle2).getPlayer());
                partyPlayers.get(bucle2).getPlayer().showPlayer(player);

                bucle2++;
            }
        }
        //player.getWorld().refreshChunk(player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ());
        //player.getWorld().loadChunk(player.getLocation().getChunk());

    }


    public static void setAllPartyXp(Jumps jump, int nb, int max) {
        ArrayList<JumpsPlayer> players = new ArrayList<>();
        players.addAll(jump.getPlayers().values());
        int bucle = 0;
        while (bucle < players.size()) {
            //players.get(bucle).getPlayer().setLevel(nb);
            UtilPlayer.setXpBarCount(players.get(bucle).getPlayer(), nb, (float) (100.0F*nb)/max);
            bucle++;
        }
    }

    public static void setAllPartyXp(Player player, int nb, int max) {
        //player.setLevel(nb);
        UtilPlayer.setXpBarCount(player, nb, (float) (100.0F*nb)/max);
    }

    public static void sendSecondTitle(Jumps jump, String sc) {
        ArrayList<JumpsPlayer> players = new ArrayList<>();
        players.addAll(jump.getPlayers().values());
        int bucle = 0;
        while (bucle < players.size()) {
            //TitleAPI.sendTitle(players.get(bucle).getPlayer(), 0, 20, 0, "§2§l" + sc, "");
            UtilTitle.sendTitle(new Title(EnumWrappers.TitleAction.TITLE, new ChatText("§2§l" + sc), 0, 20, 0), players.get(bucle).getPlayer());
            bucle++;
        }
    }
    public static void sendSecondTitle(Player player, String sc) {
        //TitleAPI.sendTitle(player, 0, 20, 0, "§2§l" + sc, "");
        UtilTitle.sendTitle(new Title(EnumWrappers.TitleAction.TITLE, new ChatText("§2§l" + sc), 0, 20, 0), player);
    }

    public static void actionBarSecond(Jumps jump, int sc) {
        String action[] = {"§4▇","§4▇","§4▇","§4▇","§4▇","§4▇","§4▇","§4▇","§4▇","§4▇"};

        switch (sc) {
            case 10 :
                action[0] = "§2▇";
                break;

            case 9 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                break;

            case 8 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                break;

            case 7 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                action[3] = "§2▇";
                break;

            case 6 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                action[3] = "§2▇";
                action[4] = "§2▇";
                break;

            case 5 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                action[3] = "§2▇";
                action[4] = "§2▇";
                action[5] = "§2▇";
                break;

            case 4 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                action[3] = "§2▇";
                action[4] = "§2▇";
                action[5] = "§2▇";
                action[6] = "§2▇";
                break;

            case 3 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                action[3] = "§2▇";
                action[4] = "§2▇";
                action[5] = "§2▇";
                action[6] = "§2▇";
                action[7] = "§2▇";
                break;

            case 2 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                action[3] = "§2▇";
                action[4] = "§2▇";
                action[5] = "§2▇";
                action[6] = "§2▇";
                action[7] = "§2▇";
                action[8] = "§2▇";
                break;

            case 1 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                action[3] = "§2▇";
                action[4] = "§2▇";
                action[5] = "§2▇";
                action[6] = "§2▇";
                action[7] = "§2▇";
                action[8] = "§2▇";
                action[9] = "§2▇";
                break;
            case -1 :
                action[0] = "";
                action[1] = "";
                action[2] = "";
                action[3] = "";
                action[4] = "";
                action[5] = "";
                action[6] = "";
                action[7] = "";
                action[8] = "";
                action[9] = "";
                break;
            default:
                action = action;
        }

        String msg = "";

        int bucle1 = 0;
        while (bucle1 < action.length) {
            msg = msg+action[bucle1];
            bucle1++;
        }

        ArrayList<JumpsPlayer> players = new ArrayList<>();
        players.addAll(jump.getPlayers().values());
        int bucle = 0;
        while (bucle < players.size()) {
            //Action.playAction(players.get(bucle).getPlayer(), msg);
            UtilChat.sendActionBar(new ChatText(msg), players.get(bucle).getPlayer());
            bucle++;
        }
    }
    public static void actionBarSecond(Player player, int sc) {
        String action[] = {"§4▇","§4▇","§4▇","§4▇","§4▇","§4▇","§4▇","§4▇","§4▇","§4▇"};

        switch (sc) {
            case 10 :
                action[0] = "§2▇";
                break;

            case 9 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                break;

            case 8 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                break;

            case 7 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                action[3] = "§2▇";
                break;

            case 6 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                action[3] = "§2▇";
                action[4] = "§2▇";
                break;

            case 5 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                action[3] = "§2▇";
                action[4] = "§2▇";
                action[5] = "§2▇";
                break;

            case 4 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                action[3] = "§2▇";
                action[4] = "§2▇";
                action[5] = "§2▇";
                action[6] = "§2▇";
                break;

            case 3 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                action[3] = "§2▇";
                action[4] = "§2▇";
                action[5] = "§2▇";
                action[6] = "§2▇";
                action[7] = "§2▇";
                break;

            case 2 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                action[3] = "§2▇";
                action[4] = "§2▇";
                action[5] = "§2▇";
                action[6] = "§2▇";
                action[7] = "§2▇";
                action[8] = "§2▇";
                break;

            case 1 :
                action[0] = "§2▇";
                action[1] = "§2▇";
                action[2] = "§2▇";
                action[3] = "§2▇";
                action[4] = "§2▇";
                action[5] = "§2▇";
                action[6] = "§2▇";
                action[7] = "§2▇";
                action[8] = "§2▇";
                action[9] = "§2▇";
                break;
            case -1 :
                action[0] = "";
                action[1] = "";
                action[2] = "";
                action[3] = "";
                action[4] = "";
                action[5] = "";
                action[6] = "";
                action[7] = "";
                action[8] = "";
                action[9] = "";
                break;
            default:
                action = action;
        }

        String msg = "";

        int bucle1 = 0;
        while (bucle1 < action.length) {
            msg = msg+action[bucle1];
            bucle1++;
        }

        UtilChat.sendActionBar(new ChatText(msg), player);

        //Action.playAction(player, msg);
        //ActionBarAPI.sendActionBar(player, action.toString());
    }

}
