package com.masterpik.jumpsrun.party;

import com.masterpik.api.util.UtilDate;
import com.masterpik.api.util.UtilInteger;
import com.masterpik.connect.api.PlayersStatistics;
import com.masterpik.connect.enums.Statistics;
import com.masterpik.connect.enums.StatsPiks;
import com.masterpik.connect.enums.StatsPlayers;
import com.masterpik.jumpsrun.Settings;
import com.masterpik.jumpsrun.chat.Messages;

import java.util.ArrayList;
import java.util.UUID;

public class JumpsStatistics {

    public static ArrayList<Integer> getPiks(JumpsPlayer player) {
        Jumps jump = player.getJumps();
        ArrayList<Integer> piks = new ArrayList<>();
        piks.add(StatsPiks.PARTY.getPiks());
        piks.add(StatsPiks.JUMPSRUN.getPiks());
        piks.add(jump.getLevel().getTyp().getPiks());
        piks.add(jump.getLevel().getLvl().getPiks());
        piks.add(jump.getStartPlayersCount());
        return piks;
    }

    public static int getPiksWin(JumpsPlayer player, boolean winStatu) {
        return PlayersStatistics.getPikPointsWin(UtilInteger.addInts(getPiks(player)), winStatu);
    }

    public static int getPiksWin(JumpsPlayer player) {
        return UtilInteger.addInts(getPiks(player));
    }

    public static void playerQuit(JumpsPlayer player) {
        Jumps jump = player.getJumps();

        UUID uuid = player.getPlayer().getUniqueId();

        if (jump.isStart()) {

            long diff;

            if (jump.isWin()) {

                diff = jump.getEndTime()-jump.getStartTime();

                if (player.isWin()) {
                    PlayersStatistics.incrementStatistic(StatsPlayers.JUMPSRUN, uuid, Statistics.WIN_AMOUNT);
                } else {
                    PlayersStatistics.incrementStatistic(StatsPlayers.JUMPSRUN, uuid, Statistics.LOST_AMOUNT);
                }

                PlayersStatistics.addPikPoint(uuid, getPiksWin(player), StatsPlayers.JUMPSRUN, player.isWin());

            } else {

                diff = UtilDate.getCurrentDate().getTime()-jump.getStartTime();

            }
            PlayersStatistics.addSpentTime(uuid, diff, StatsPlayers.JUMPSRUN);

            PlayersStatistics.incrementStatistic(StatsPlayers.JUMPSRUN, uuid, Statistics.GAMES_AMOUNT);


        }
    }

}
