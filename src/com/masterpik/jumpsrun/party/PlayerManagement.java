package com.masterpik.jumpsrun.party;

import com.masterpik.api.specialEffects.Fireworks;
import com.masterpik.api.util.UtilPlayer;
import com.masterpik.jumpsrun.Main;
import com.masterpik.jumpsrun.Settings;
import com.masterpik.jumpsrun.Utils;
import com.masterpik.jumpsrun.bungee.BungeeWork;
import com.masterpik.jumpsrun.chat.ChatManagement;
import com.masterpik.jumpsrun.chat.Messages;
import com.masterpik.jumpsrun.genbojects.LevelManagement;
import com.masterpik.jumpsrun.items.LobbyItems;
import com.masterpik.jumpsrun.scoreboard.ScoreboardManagement;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.HashMap;

public class PlayerManagement {

    public static HashMap<Player, JumpsPlayer> players;

    public static void PlayersInit() {
        players = new HashMap<>();

    }

    public static boolean isRegister(Player player) {
        boolean reg = false;

        if (PlayerManagement.players != null) {
            reg = PlayerManagement.players.containsKey(player);
        }

        return reg;
    }

    public static JumpsPlayer playerToJumpsPlayer(Player player) {
        if (PlayerManagement.isRegister(player)) {
            return players.get(player);
        }
        else {
            return null;
        }
    }

    public static void playerJoin(Player player) {

        JumpsPlayer Jplayer = PlayerManagement.playerToJumpsPlayer(player);
        Jumps jump = Jplayer.getJumps();

        player.setGameMode(GameMode.ADVENTURE);
        UtilPlayer.realPlayerClear(player);

        if (jump.getPlayers().size() == 2) {
            JumpsManagement.startJumps(jump);
        }

        player.teleport(Settings.lobbyLocation);

        ChatManagement.sendMessage("@" + Messages.Join, player);

        if (jump.getPlayers().size() == 1) {
            ChatManagement.brodcasteMessage(Messages.notEnoughtPlayers, player);
        }

        Utils.showPlayersPlayer(player);


        Utils.setAllPartyXp(jump, 0, Settings.startCountDown);

        player.getInventory().setItem(8, LobbyItems.lobbyItemHub);

        player.setScoreboard(ScoreboardManagement.getMain(player));
        ScoreboardManagement.addAPlayerToParty(jump, Jplayer);

        if (Main.spectators != null || Main.spectators.size() != 0) {
            int bcl = 0;
            while (bcl < Main.spectators.size()) {
                player.hidePlayer(Main.spectators.get(bcl));
                Main.spectators.get(bcl).showPlayer(player);
                bcl++;
            }
        }

    }

    public static void playerQuit(Player player) {
         if (PlayerManagement.players.containsKey(player)) {
            JumpsPlayer Jplayer = PlayerManagement.playerToJumpsPlayer(player);

            Jumps jump = Jplayer.getJumps();

            ChatManagement.sendMessage("@" + Messages.Quit, player);

             JumpsStatistics.playerQuit(Jplayer);

            ArrayList<String> retur = new ArrayList<>();
            retur.add(0, "hub");
            retur.add(1, "jumpsrun");
            retur.add(2, Integer.toString(Jplayer.getJumps().getLevel().getType()));
            retur.add(3, Integer.toString(Jplayer.getJumps().getLevel().getDifficulty()));

            BungeeWork.sendToGameMessage(player, retur, "hubCo");

            jump.getPlayers().remove(player);

            PlayerManagement.players.remove(player);

            if (jump.getPlayers() != null
                    && jump.getPlayers().size() == 1
                    && !jump.isEnd()) {
                JumpsManagement.winJumps(jump, (JumpsPlayer) jump.getPlayers().values().toArray()[0]);
            }

            if (jump.getPlayers().size() == 0
                    || jump.getPlayers() == null) {
                JumpsManagement.deleteJump(jump);
            } else {
                ScoreboardManagement.removeAPlayerToParty(jump, Jplayer);
            }

            if (Main.spectators.contains(player)) {
                Main.spectators.remove(player);
            }
        } else {
             if (Main.spectators.contains(player)) {
                 Main.spectators.remove(player);
             }
         }


    }

    public static void playerWin(JumpsPlayer Jplayer) {


        Jumps jump = Jplayer.getJumps();

        if (!Jplayer.isWin()
                && jump.getWinner() == null) {

            Jplayer.setWin(true);

            jump.setWinner(Jplayer);


            JumpsManagement.winJumps(jump, Jplayer);

        }

    }


}
