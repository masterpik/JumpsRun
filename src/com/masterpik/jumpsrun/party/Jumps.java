package com.masterpik.jumpsrun.party;

import com.masterpik.jumpsrun.genbojects.Level;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;

public class Jumps {

    String name;

    World world;

    Level level;

    int lenght;

    ArrayList<Block> blocks;

    HashMap<Player, JumpsPlayer> players;

    boolean countDown;
    boolean start;
    boolean end;
    boolean forced;
    boolean win;

    JumpsPlayer winner;

    int countDownNb;

    long startTime;
    long endTime;

    int startPlayersCount;

    public Jumps(String Pname, World Pworld, Level Plevel) {
        this.name = Pname;

        this.world = Pworld;

        this.level = Plevel;

        this.blocks = new ArrayList<>();

        this.players = new HashMap<>();

        this.countDown = false;
        this.start = false;
        this.end = false;
        this.forced = false;
        this.win = false;

        this.winner = null;

        this.countDownNb = 0;
        
        this.startTime = 0;
        this.endTime = 0;

        this.startPlayersCount = 0;

    }

    public String getName() {
        return name;
    }
    public void setName(String Pname) {
        this.name = Pname;
    }

    public World getWorld() {
        return world;
    }
    public void setWorld(World Pworld) {
        this.world = Pworld;
    }

    public Level getLevel() {
        return level;
    }
    public void setLevel(Level Plevel) {
        this.level = Plevel;
    }

    public int getLenght() {
        return lenght;
    }
    public void setLenght(int Plenght) {
        this.lenght = Plenght;
    }

    public ArrayList<Block> getBlocks() {
        return blocks;
    }
    public void setBlocks(ArrayList<Block> Pblocks) {
        this.blocks = Pblocks;
    }

    public HashMap<Player, JumpsPlayer> getPlayers() {
        return players;
    }
    public void setPlayers(HashMap<Player, JumpsPlayer> Pplayers) {
        this.players = Pplayers;
    }


    public boolean isCountDown() {
        return countDown;
    }
    public void setCountDown(boolean PcountDown) {
        this.countDown = PcountDown;
    }

    public boolean isStart() {
        return start;
    }
    public void setStart(boolean Pstart) {
        this.start = Pstart;
    }

    public boolean isEnd() {
        return end;
    }
    public void setEnd(boolean Pend) {
        this.end = Pend;
    }

    public boolean isForced() {
        return forced;
    }
    public void setForced(boolean Pforced) {
        this.forced = Pforced;
    }

    public boolean isWin() {
        return win;
    }
    public void setWin(boolean win) {
        this.win = win;
    }



    public JumpsPlayer getWinner() {
        return winner;
    }
    public void setWinner(JumpsPlayer Pwinner) {
        this.winner = Pwinner;
    }

    public int getCountDownNb() {
        return countDownNb;
    }
    public void setCountDownNb(int PcountDownNb) {
        this.countDownNb = PcountDownNb;
    }


    public long getEndTime() {
        return endTime;
    }
    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public long getStartTime() {
        return startTime;
    }
    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public int getStartPlayersCount() {
        return startPlayersCount;
    }
    public void setStartPlayersCount(int startPlayersCount) {
        this.startPlayersCount = startPlayersCount;
    }
}
