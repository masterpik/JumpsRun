package com.masterpik.jumpsrun.party;

import com.masterpik.api.specialEffects.Fireworks;
import com.masterpik.api.util.UtilDate;
import com.masterpik.api.util.UtilPlayer;
import com.masterpik.api.util.UtilWorld;
import com.masterpik.connect.enums.Statistics;
import com.masterpik.connect.enums.Times;
import com.masterpik.jumpsrun.Main;
import com.masterpik.jumpsrun.Settings;
import com.masterpik.jumpsrun.Utils;
import com.masterpik.jumpsrun.bungee.BungeeWork;
import com.masterpik.jumpsrun.chat.ChatManagement;
import com.masterpik.jumpsrun.chat.Messages;
import com.masterpik.jumpsrun.genbojects.Generator;
import com.masterpik.jumpsrun.genbojects.Level;
import com.masterpik.jumpsrun.items.JumpsItems;
import org.bukkit.*;
import org.bukkit.Bukkit;
import org.bukkit.FireworkEffect;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

public class JumpsManagement {

    public static HashMap<String, Jumps> jumps;

    public static void JumpsInit() {
        jumps = new HashMap<>();
    }

    public static Jumps createJump(String name, Level level) {

        UtilWorld.cloneWorld("default", name);

        Jumps jump = new Jumps(name, Bukkit.getWorld(name), level);

        jumps.put(name, jump);

        JumpsManagement.initGamerule(jump.getWorld());

        return jump;
    }

    public static void generateJump(Jumps jump) {

        Generator.generateAJumps(jump);

    }

    public static void startJumps(Jumps jump) {

        jump.setCountDownNb(0);

        if (jump.isForced()) {
            jump.setCountDownNb(10);
            jump.setStart(true);
            jump.setCountDown(false);
        } else {
            jump.setCountDownNb(Settings.startCountDown);
            jump.setCountDown(true);
            jump.setStart(false);
        }


        ArrayList<Integer> in = new ArrayList<>();

        in.add((Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {

            @Override
            public void run() {

                if (jump.isForced()
                        || jump.getPlayers().size() >= 2) {

                    //Utils.setAllPartyXp(jump, jump.getCountDownNb(), Settings.startCountDown);


                    if (jump.getCountDownNb() > 10) {
                        if ((jump.getCountDownNb()%10) == 0) {
                            ChatManagement.brodcasteMessage("" + Messages.countRebour + "" + jump.getCountDownNb() + " §r§a§lsecondes", ((JumpsPlayer)jump.getPlayers().values().toArray()[0]).getPlayer());
                            int bucl = 0;
                            while (bucl < jump.getPlayers().keySet().size()) {

                                ((Player)jump.getPlayers().keySet().toArray()[bucl]).playSound(((Player) jump.getPlayers().keySet().toArray()[bucl]).getLocation(), Sound.BLOCK_NOTE_PLING, Settings.volumeSound, Settings.pitchSound);
                                bucl++;
                            }
                        }
                    } else if (jump.getCountDownNb() == 0) {

                        ChatManagement.brodcasteMessage(Messages.partyStart, ((JumpsPlayer)jump.getPlayers().values().toArray()[0]).getPlayer());

                        jump.setStartTime(UtilDate.getCurrentDate().getTime());
                        jump.setStartPlayersCount(jump.players.size());

                        int bucle = 0;
                        ArrayList<JumpsPlayer> players = new ArrayList<JumpsPlayer>();
                        players.addAll(jump.getPlayers().values());
                        while (bucle < players.size()) {

                            Location tpLoc = Settings.firstLocation.clone();
                            tpLoc.setWorld(players.get(bucle).getJumps().getWorld());
                            tpLoc.add(0, 1, 0);
                            players.get(bucle).getPlayer().teleport(tpLoc);
                            UtilPlayer.realPlayerClear(players.get(bucle).getPlayer());

                            players.get(bucle).getPlayer().getInventory().setItem(4, JumpsItems.hidePlayer);

                            players.get(bucle).getPlayer().playSound(players.get(bucle).getPlayer().getLocation(), Sound.BLOCK_NOTE_PLING, Settings.volumeSound, Settings.pitchSound + 1);

                            players.get(bucle).getPlayer().sendMessage(Messages.startingMessageDescription);

                            bucle++;
                        }

                        Bukkit.getScheduler().cancelTask(in.get(0));
                        //Utils.setAllPartyXp(jump, 0, Settings.startCountDown);
                        Utils.sendSecondTitle(jump, Messages.partyStartTitle);
                        Utils.actionBarSecond(jump, -1);
                    } else {
                        jump.setStart(true);
                        jump.setCountDown(false);

                        Utils.sendSecondTitle(jump, Integer.toString(jump.getCountDownNb()));
                        Utils.actionBarSecond(jump, jump.getCountDownNb());

                        if (jump.getCountDownNb() > 1) {
                            if ((jump.getCountDownNb()%10) == 0) {
                                ChatManagement.brodcasteMessage("" + Messages.countRebour + "" + jump.getCountDownNb() + " §r§a§lsecondes", ((JumpsPlayer) jump.getPlayers().values().toArray()[0]).getPlayer());
                            }
                            int bucl = 0;
                            while (bucl < jump.getPlayers().keySet().size()) {

                                ((Player)jump.getPlayers().keySet().toArray()[bucl]).playSound(((Player) jump.getPlayers().keySet().toArray()[bucl]).getLocation(), Sound.BLOCK_NOTE_PLING, Settings.volumeSound, Settings.pitchSound);
                                bucl++;
                            }
                        } else if (jump.getCountDownNb() == 1) {
                            if ((jump.getCountDownNb()%10) == 0) {
                                ChatManagement.brodcasteMessage("" + Messages.countRebour + "" + jump.getCountDownNb() + " §r§a§lseconde", ((JumpsPlayer) jump.getPlayers().values().toArray()[0]).getPlayer());
                            }
                            int bucl = 0;
                            while (bucl < jump.getPlayers().keySet().size()) {

                                ((Player)jump.getPlayers().keySet().toArray()[bucl]).playSound(((Player) jump.getPlayers().keySet().toArray()[bucl]).getLocation(), Sound.BLOCK_NOTE_PLING, Settings.volumeSound, Settings.pitchSound);
                                bucl++;
                            }
                        }

                    }

                    if (jump.getCountDownNb() > 0) {
                        jump.setCountDownNb(jump.getCountDownNb()-1);
                    }


                } else {
                    //Utils.setAllPartyXp(jump, 0, Settings.startCountDown);
                    jump.setStart(false);
                    jump.setCountDown(false);

                    Bukkit.getScheduler().cancelTask(in.get(0));
                }

            }
        }, 0, 20)));

    }

    public static void deleteJump(Jumps jump) {

        int bucl = 0;
        while (bucl < Main.spectators.size()) {
            //Bukkit.getPlayer("axroy").sendRawMessage(Main.spectators.get(bucl).getWorld().getName()+" | "+next);
            if (Main.spectators.get(bucl).getWorld().getName().equalsIgnoreCase(jump.getWorld().getName())) {
                Main.spectators.get(bucl).teleport(Settings.lobbyLocation);
            }
            bucl ++;
        }


        UtilWorld.deleteWorld(jump.getName());



        Bukkit.getScheduler().runTaskLater(com.masterpik.api.Main.plugin, new Runnable() {
            @Override
            public void run() {
                jumps.remove(jump.getName());
            }
        }, 20L);

    }

    public static int getNext(Level level) {

        int bucle1 = 0;
        boolean scdBcl = false;

        boolean takeLast = false;

        ArrayList<Jumps> parti = new ArrayList<>();
        ArrayList<Jumps> parti2 = new ArrayList<>();
        parti2.addAll(jumps.values());

        int bucle = 0;

        while (bucle < parti2.size()) {

            if (parti2.get(bucle).getLevel() == level) {
                parti.add(parti2.get(bucle));
            }

            bucle ++ ;
        }

        if (parti.isEmpty()) {
            takeLast = true;
        } else {

            while (bucle1 < parti.size()) {

                if (!scdBcl) {
                    if (!parti.get(bucle1).isStart()) {
                        return Integer.parseInt(parti.get(bucle1).getName());
                    } else if (bucle1 == (parti.size() - 1)) {
                        bucle1--;
                        scdBcl = true;
                    }
                } else {
                    if(!jumps.containsKey(""+Integer.parseInt(parti.get(bucle1).getName())+1)) {
                        return Integer.parseInt(parti.get(bucle1).getName())+1;
                    }

                }

                bucle1++;
            }

        }


        if (takeLast) {
            int bucle3 = 1;
            boolean ok2 = false;

            while (!ok2) {

                if(!jumps.containsKey(""+bucle3)) {
                    ok2 = true;
                    return bucle3;
                }

                bucle3++;
            }
        }
        return 1;
    }

    public static boolean isPartyExist(String name) {

        boolean isExist = false;

        int bucle = 0;

        ArrayList<Jumps> list = new ArrayList<>();
        list.addAll(JumpsManagement.jumps.values());

        while (bucle < list.size()) {

            if (list.get(bucle).getName().equals(name)) {
                isExist = true;
            }

            bucle++;
        }

        return isExist;
    }

    public static void winJumps(Jumps jump, JumpsPlayer player) {


        ArrayList<JumpsPlayer> players = new ArrayList<>();
        players.addAll(jump.getPlayers().values());

        if (!player.getPlayer().getWorld().equals(Bukkit.getWorld("lobby"))
                || !players.get(0).getPlayer().getWorld().equals(Bukkit.getWorld("lobby"))) {

            int bucl = 0;
            while (bucl < Main.spectators.size()) {
                //Bukkit.getPlayer("axroy").sendRawMessage(Main.spectators.get(bucl).getWorld().getName()+" | "+next);
                if (Main.spectators.get(bucl).getWorld().getName().equalsIgnoreCase(jump.getWorld().getName())) {
                    Main.spectators.get(bucl).teleport(Settings.lobbyLocation);
                }
                bucl ++;
            }

            ChatManagement.brodcasteMessage("" + Messages.win1 + "§r§6" + player.getPlayer().getName() + "" + Messages.win2 + "", player.getPlayer());


            jump.setWin(true);
            jump.setEndTime(UtilDate.getCurrentDate().getTime());

            ArrayList<Player> BukkitPlayers = new ArrayList<>();

            for (JumpsPlayer pl : players) {
                BukkitPlayers.add(pl.getPlayer());
            }

            for (JumpsPlayer pl : players) {
                pl.getPlayer().setGameMode(GameMode.SPECTATOR);
                UtilPlayer.showPlayer(pl.getPlayer(), BukkitPlayers);

                pl.getPlayer().sendMessage("\n"+Messages.main+(Statistics.PIK.getMessage().replaceAll("%v", Integer.toString(JumpsStatistics.getPiksWin(pl, pl.isWin())))));
                pl.getPlayer().sendMessage(Messages.main+(Statistics.SPENT_TIME.getMessage().replaceAll("%v", Times.getTime(UtilDate.millisecondsToMinutes(jump.getEndTime() - jump.getStartTime())))));
                pl.getPlayer().sendMessage(Messages.main+Messages.statsRegister+"\n ");
            }



            Fireworks.explodeFirework(
                    player.getPlayer().getLocation(),
                    FireworkEffect.Type.BALL_LARGE,
                    new ArrayList() {{
                        add(Color.PURPLE);
                        add(Color.LIME);
                        add(Color.YELLOW);
                        add(Color.AQUA);
                        add(Color.ORANGE);
                    }},
                    new ArrayList() {{
                        add(Color.MAROON);
                        add(Color.BLUE);
                        add(Color.FUCHSIA);
                        add(Color.ORANGE);
                    }},
                    true,
                    false);

            jump.setEnd(true);

            BukkitTask time = Bukkit.getServer().getScheduler().runTaskLater(Main.plugin, new Runnable() {
                @Override
                public void run() {
                    int bucle = 0;

                    if (jump.isEnd()) {

                        while (bucle < players.size()) {

                            players.get(bucle).getPlayer().sendMessage(Messages.endParty);
                            BungeeWork.sendPlayer(players.get(bucle).getPlayer(), "hub");

                            bucle++;
                        }

                    }
                }
            }, 100);
        }

    }

    public static void initGamerule(World world) {
        String trou = "true";
        String folse = "false";

        world.setFullTime(6000L);

        world.setGameRuleValue("commandBlockOutput", folse);
        world.setGameRuleValue("doDaylightCycle", folse);
        world.setGameRuleValue("doEntityDrops", folse);
        world.setGameRuleValue("doFireTick", folse);
        world.setGameRuleValue("doMobLoot", folse);
        world.setGameRuleValue("doMobSpawning", folse);
        world.setGameRuleValue("doTileDrops", trou);
        world.setGameRuleValue("keepInventory", folse);
        world.setGameRuleValue("logAdminCommands", trou);
        world.setGameRuleValue("mobGriefing", folse);
        world.setGameRuleValue("naturalRegeneration", trou);
        world.setGameRuleValue("randomTickSpeed", "3");
        world.setGameRuleValue("reducedDebugInfo", trou);
        world.setGameRuleValue("sendCommandFeedback", folse);
        world.setGameRuleValue("showDeathMessages", folse);

        world.setAmbientSpawnLimit(0);
        world.setAnimalSpawnLimit(0);
        world.setMonsterSpawnLimit(0);
        world.setWaterAnimalSpawnLimit(0);

        world.setAutoSave(false);

        if (world.getName() == "lobby") {
            world.setPVP(false);
            world.setDifficulty(Difficulty.PEACEFUL);
        } else {
            world.setDifficulty(Difficulty.PEACEFUL);
            world.setPVP(false);
        }
    }

}
