package com.masterpik.jumpsrun.party;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class JumpsPlayer {

    Player player;
    Jumps jumps;

    boolean win;

    Block lastBlock;

    public JumpsPlayer(Player Pplayer, Jumps Pjumps) {
        this.player = Pplayer;
        this.jumps = Pjumps;

        this.win = false;

        this.lastBlock = null;
    }

    public Player getPlayer() {
        return player;
    }
    public void setPlayer(Player Pplayer) {
        this.player = Pplayer;
    }

    public Jumps getJumps() {
        return jumps;
    }
    public void setJumps(Jumps Pjumps) {
        this.jumps = Pjumps;
    }

    public boolean isWin() {
        return win;
    }
    public void setWin(boolean Pwin) {
        this.win = Pwin;
    }

    public Block getLastBlock() {
        return lastBlock;
    }
    public void setLastBlock(Block PlastBlock) {
        this.lastBlock = PlastBlock;
    }
}
